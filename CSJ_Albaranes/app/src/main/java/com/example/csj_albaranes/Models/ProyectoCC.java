package com.example.csj_albaranes.Models;

public class ProyectoCC {
    public String description;

    public ProyectoCC(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
